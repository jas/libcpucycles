Source: libcpucycles
Section: devel
Priority: optional
Maintainer: Miguel Landaeta <nomadium@debian.org>
Uploaders:
 Nick Black <dankamongmen@gmail.com>,
 Simon Josefsson <simon@josefsson.org>,
 Jan Mojžíš <jan.mojzis@gmail.com>,
Build-Depends:
 debhelper (>= 13~),
 debhelper-compat (= 13),
 python3,
Standards-Version: 4.7.2
Homepage: https://cpucycles.cr.yp.to/
Vcs-Git: https://salsa.debian.org/debian/libcpucycles.git
Vcs-Browser: https://salsa.debian.org/debian/libcpucycles
Rules-Requires-Root: no

Package: cpucycles
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: command-line tool for counting CPU cycles (libcpucycles)
 libcpucycles provides a simple API to access hardware precise timers
 to understand and improve software performance.
 .
 libcpucycles understands machine-level cycle counters for most architectures.
 libcpucycles also understands common OS-level mechanisms, which give varying
 levels of accuracy.
 .
 This package contains the cpucycles-info command-line tool.

Package: libcpucycles-dev
Section: libdevel
Multi-Arch: same
Architecture: any
Depends:
 libcpucycles1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: library for counting CPU cycles - development files
 libcpucycles provides a simple API to access hardware precise timers
 to understand and improve software performance.
 .
 libcpucycles understands machine-level cycle counters for most architectures.
 libcpucycles also understands common OS-level mechanisms, which give varying
 levels of accuracy.
 .
 This package contains development files and static library.

Package: libcpucycles1
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: library for counting CPU cycles
 libcpucycles provides a simple API to access hardware precise timers
 to understand and improve software performance.
 .
 libcpucycles understands machine-level cycle counters for most architectures.
 libcpucycles also understands common OS-level mechanisms, which give varying
 levels of accuracy.
 .
 This package contains the shared library.
